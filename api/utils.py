from django.core.cache import cache
from geopy.distance import geodesic
from typing import Dict
from random import randint


def create_route() -> int:
    route_id = randint(1, 10)

    return route_id


def populate_route(route_id: int, data_point: Dict) -> bool:
    success = True
    wgs84_coordinates = cache.get(route_id)

    try:
        if wgs84_coordinates is not None:
            if not any(old_data_point == data_point for old_data_point in wgs84_coordinates):
                wgs84_coordinates.append(data_point)
                cache.set(route_id, wgs84_coordinates)
        else:
            wgs84_coordinates = [data_point]
            cache.set(route_id, wgs84_coordinates)
    except EnvironmentError:
        raise EnvironmentError(f'Please start you memcached server if it is not started yet.')

    return success


def calculate_route_length(route_id: int) -> float:
    wgs84_coordinates = cache.get(route_id)

    paths = []

    for x in range(len(wgs84_coordinates) - 1):
        point_1 = wgs84_coordinates[x]
        point_2 = wgs84_coordinates[x + 1]

        point_1_lat = point_1['lat']
        point_1_lon = point_1['lon']

        point_2_lat = point_2['lat']
        point_2_lon = point_2['lon']

        origin = (point_1_lat, point_1_lon)
        dist = (point_2_lat, point_2_lon)

        paths.append(geodesic(origin, dist).kilometers)

    longest_length = sum(paths)

    return longest_length
