# Code test

## Overview
This test is designed to be small enough to not take too much time,
but loose enough that you will be able to show your own
architectural solutions as well as your code structure.

### Hypothetical background
The application is a small service, which accepts data from a GPS tracker device.
In the beginning of a track, it requests a route to be created,
then continuously populates it with data points (WGS84 coordinates).
Eventually a request to calculate the length of the route is made.

### Requirements
We have provided you with an acceptance test. This acceptance test is
a positive test for how a web server should ingest information, and output
a calculated result.

There is also a second part which is to calculate
the longest path for each day.
This information is expected to be highly requested,
and past days can't have new routes included.

### Delivery
Share a code base with us in any way that makes sense to you,
when you feel that you are done.

### Time management
We expect this task to take between one and six hours.
If you are working for more than six hours, we appreciate your enthusiasm,
but you are overdoing it. We don’t want to take up too much of your time.
We won’t be timing you.

