from django.contrib import admin
from django.urls import path

from api import views

urlpatterns = [
    path('route/', views.Route.as_view()),
    path('route/<int:route_id>/way_point/', views.WayPoint.as_view()),
    path('route/<int:route_id>/length/', views.Length.as_view()),
    path('admin/', admin.site.urls),
]
