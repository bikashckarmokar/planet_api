#### Install memcached and start. 
```
sudo apt update
sudo apt install memcached
sudo systemctl start memcached
```

### Install requirement file
```
pip install -r requirements.txt 
```

### Demo Run
![Image_Caption](demo.gif)