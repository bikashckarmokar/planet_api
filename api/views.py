from rest_framework.views import APIView
from rest_framework.response import Response

from api import utils


class Route(APIView):
    def post(self, request):
        route_id = utils.create_route()

        response = {
            'route_id': route_id
        }

        return Response(response)


class WayPoint(APIView):
    def post(self, request, route_id):
        data = request.data
        success = utils.populate_route(route_id, data)

        response = {
            'populated': success
        }

        return Response(response)


class Length(APIView):
    def get(self, request, route_id):
        route_length = utils.calculate_route_length(route_id)

        response = {
            'km': route_length
        }

        return Response(response)

